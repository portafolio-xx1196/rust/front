mod products;

use products::Products;
use yew::prelude::*;

#[function_component]
fn App() -> Html {

    html! {
        <div class="container">
            <h1 class="title">{"Yew productsApp"}</h1>
            <Products/>
        </div>
    }
}

fn main() {
    yew::Renderer::<App>::new().render();
}